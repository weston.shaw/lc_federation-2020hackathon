# LC Federation 2020 Hackathon

## Ideas

* Yearly Retro App
    - Track your progress through the year - enter your goals and create milestones
    - Collaborate with your RM on progress toward goals, suggestions and concerns as the year goes on
    - Gamification? - Earn badges, set personal goals or ... ?
* A game
  * Gif Charades
    - Users submit a line of text with a list of gifs attached - "Create a Card"
    - A single User can start a group session by inviting users to a url and selecting a card
    - The URL hosts the discussion and guesses as to what the gifs are trying to spell out
    - In the end a winner emerges and users can rate the card for other potential players
